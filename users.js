const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'users';
const assert = require('assert');
const jwt = require('jsonwebtoken');

exports.login = (req, res) => {
  if(!res.isValid){
    res.json({token:false});
  } else {
    res.json({token: res.token})
  }
}
 
exports.checkUser = (req, res, next) => {
  try{
    MongoClient.connect(url, function(err, client){
      assert.strictEqual(null, err);
       
      const db = client.db(dbName);
      const collection = db.collection('users');

      console.log(`looking up ${req.body.user} ${req.body.pw}`)
      
      collection.find({name: req.body.user, pw: req.body.pw}).toArray().then(docs => {
        if(docs.length === 1){
          let token = jwt.sign({name: req.body.user},'blueball',{expiresIn: '5s'});
            res.token = token;
            res.isValid = true;
            next();
            return;
        } else {
          res.isValid = false;
          next();
          return;
        };
      });
  
    })
  } catch( err ) {
  } finally {
  }
}

 