const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');

const port = 3000;
const appDir = path.dirname(require.main.filename);
const publicDir = path.join(__dirname,'../'+'client/build');

const Users = require('./users')

//Middleware
app.use(express.static(publicDir))
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/', (req, res, next) => {
  res.send('load index here')
  // res.sendFile(path.join(__dirname,'../'+publicDir+'/index.html'));
})

//Login
app.post('/login', Users.checkUser, Users.login);

app.listen(port, ()=> {
  console.log('Listening to port 3000');
});
